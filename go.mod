module github.com/gogf/gf

go 1.14

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/clbanning/mxj v1.8.5-0.20200714211355-ff02cfb8ea28
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/fatih/color v1.12.0
	github.com/fsnotify/fsnotify v1.4.9
	github.com/gogf/mysql v1.6.1-0.20210603073548-16164ae25579
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/gorilla/websocket v1.4.2
	github.com/grokify/html-strip-tags-go v0.0.0-20190921062105-daaa06bf1aaf
	github.com/kr/text v0.2.0 // indirect
	github.com/mattn/go-runewidth v0.0.10 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/olekukonko/tablewriter v0.0.5
	go.opentelemetry.io/otel v1.0.0-RC2
	go.opentelemetry.io/otel/oteltest v1.0.0-RC2
	go.opentelemetry.io/otel/trace v1.0.0-RC2
	golang.org/x/net v0.0.0-20210520170846-37e1c6afe023
	golang.org/x/sys v0.0.0-20210616094352-59db8d763f22 // indirect
	golang.org/x/text v0.3.6
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
